package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class S2A2 {
    public static void main(String[] args) {

        int[] prime = new int[5];

        prime[0] = 2;
        prime[1] = 3;
        prime[2] = 5;
        prime[3] = 7;
        prime[4] = 11;

        System.out.println("The First Prime Number Is " + prime[0]);
        System.out.println("The Second Prime Number Is " + prime[1]);
        System.out.println("The Third Prime Number Is " + prime[2]);
        System.out.println("The Fourth Prime Number Is " + prime[3]);
        System.out.println("The Fifth Prime Number Is " + prime[4]);

        ArrayList<String> friends = new ArrayList<String>();

        friends.add("Chloe");
        friends.add("Jay");
        friends.add("Jordan");
        friends.add("Mark");

        System.out.println("My friends are: " + friends);

        HashMap<String, Integer> hygiene = new HashMap<String, Integer>() {
            {
                put("toothpaste", 15);
                put("toothbrush", 20);
                put("soap", 12);
            }
        };

        System.out.println("Our Current Inventory Consists Of: " + hygiene);
    }
}

