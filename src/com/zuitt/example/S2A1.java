package com.zuitt.example;

import java.util.Scanner;

public class S2A1 {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);

        System.out.println("Input year to be checked is a leap year");
        int yr = myObj.nextInt();

        if (yr % 4 == 0) {
            System.out.println(yr + " is a leap year");
        }
        else {
            System.out.println(yr + " is NOT a leap year");
        }
    }
}
